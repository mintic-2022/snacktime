# README #

Proyecto SnackTime

Integrantes:
Melanie Gazabon 
Juan Jose Monsalve
Juliana Cubillos
Milton Mejia
Miguel Barragan 

Definición del problema:

En Colombia, se tiene la costumbre de comer un snack entre desayuno y almuerzo o entre almuerzo y comida, donde la mayoría de estos snacks son altos en grasas y azúcares, generando problemas futuros de salubridad a los colombianos. Nosotros queremos resolver esta problemática para las actuales y futuras generaciones colombianas. Pretendemos desarrollar una página web accesible desde cualquier navegador con acceso a internet, en donde los usuarios podrán enterarse mediante audio, texto y video de los beneficios que los frutos secos pueden aportar a su salud. También se pretende brindar información de los tipos de frutos secos a vender, características de estos y adicionalmente, el futuro proyecto que llevará estos productos a las manos de nuestros clientes.